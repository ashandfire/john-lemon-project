using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public Transform muzzle;
    public Rigidbody bullet;
    public Rigidbody net;
    public int launchSpeed;
    public GameObject player;
    

    // Start is called before the first frame update
    void Start()
    {
        muzzle = GetComponent<Transform>();
        player = GetComponentInParent<GameObject>();
        //bullet = GetComponent<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Fire()
    {
        Rigidbody bulletObj;
        Debug.Log("Fire button clicked");
        bulletObj = Instantiate(bullet, muzzle.position, muzzle.rotation) as Rigidbody;
        bulletObj.AddForce(player.transform.forward * launchSpeed);
    }

    public void Net()
    {
        Rigidbody netObj;
        Debug.Log("Net button clicked");
        netObj = Instantiate(net, muzzle.position, muzzle.rotation) as Rigidbody;
        netObj.AddForce(player.transform.forward * launchSpeed);
    }
}
